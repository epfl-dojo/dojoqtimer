import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs.qml 1.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import Qt.labs.settings 1.0

Window {
    id: preferences
    width: 560
    height: 400
    title: qsTr("Preferences")

    property int currentIndex: 1

    Component.onCompleted: {
        console.log(settings.sound_source)
        if (settings.sound_source === "sounds/Alien_Siren-KevanGC-610357990.ogg") {
            currentIndex = 0
        } else if (settings.sound_source === "sounds/analog-watch-alarm_daniel-simion.ogg") {
            currentIndex = 1
        } else if (settings.sound_source === "sounds/Fire_Alarm-SoundBible.com-78848779.ogg") {
            currentIndex = 2
        } else if (settings.sound_source === "sounds/foghorn-daniel_simon.ogg") {
            currentIndex = 3
        } else if (settings.sound_source === "sounds/Loud_Alarm_Clock_Buzzer-Muk1984-493547174.ogg") {
            currentIndex = 4
        } else if (settings.sound_source === "sounds/Siren_Noise-KevanGC-1337458893.ogg") {
            currentIndex = 5
        } else {
            currentIndex = 6
        }
    }

    onCurrentIndexChanged: {
        alien_radio.checked = currentIndex == 0
        bipbip_radio.checked = currentIndex == 1
        fire_radio.checked = currentIndex == 2
        horn_radio.checked = currentIndex == 3
        buzzer_radio.checked = currentIndex == 4
        siren_radio.checked = currentIndex == 5
        //choose_radio.checked = currentIndex == 6
    }

    SpinBox {
        id: duration
        x: 348
        y: 33
        value: settings.duration
        onValueChanged: {
            console.log("Timer duration set to " + duration.value + " min")
            settings.duration = duration.value
        }
    }

    Text {
        id: duration_lbl
        x: 34
        y: 36
        text: qsTr("Duration (minutes)")
        font.pixelSize: 28
    }

    Text {
        id: ring_sound_lbl
        x: 34
        y: 85
        text: qsTr("Ring sound")
        font.pixelSize: 28
    }

    Dial {
        id: ring_volume_dial
        x: 409
        y: 212
        width: 138
        height: 117
        stepSize: 5
        to: 100
        from: 10
        value: settings.volume
        onValueChanged: settings.volume = ring_volume_dial.value
    }

//    Button {
//       id: ring_sound_btn
//       x: 35
//       y: 206
//       text: qsTr("Choose sound...")
//       onClicked: soundPath.open()
//    }
//
//    FileDialog {
//        id: soundPath
//        title: "Please chose an audio file"
//        folder: shortcuts.home
//        nameFilters: [ "Sound files (*.ogg *.mp3 *.wav)" ]
//        onAccepted: {
//            console.log("You choose " + soundPath.fileUrl)
//            ring_sound_path.text = soundPath.fileUrl
//            currentIndex = 6
//            settings.sound_source = soundPath.fileUrl
//        }
//        onRejected: {
//            console.log("cancelled")
//        }
//    }
//
//    Text {
//        id: ring_sound_path
//        x: 34
//        y: 248
//        width: 388
//        height: 15
//        text: ''
//        font.pixelSize: 12
//    }
//
//    RadioButton {
//        id: choose_radio
//        x: 191
//        y: 206
//        text: qsTr("Choose your own")
//        onClicked: {
//            console.log("Own sound choosen")
//            currentIndex = 6
//            //settings.sound_source = soundPath.fileUrls[0] || ''
//        }
//    }

    Text {
        id: ring_volume_lbl
        x: 34
        y: 269
        text: qsTr("Ring volume")
        font.pixelSize: 28
    }

    Text {
        id: ring_volume_percent
        x: 466
        y: 263
        text: Math.round(ring_volume_dial.value)  + "%"
        font.pixelSize: 12
    }



    RadioButton {
        id: alien_radio
        x: 28
        y: 124
        text: qsTr("Alien Siren")
        onClicked: {
            console.log("Sound Alien Siren selected")
            currentIndex = 0
            settings.sound_source = "sounds/Alien_Siren-KevanGC-610357990.ogg"
        }
    }

    RadioButton {
        id: bipbip_radio
        x: 28
        y: 170
        text: qsTr("Watch BipBip")
        onClicked: {
            console.log("Sound BipBip selected")
            currentIndex = 1
            settings.sound_source = "sounds/analog-watch-alarm_daniel-simion.ogg"
        }
    }

    RadioButton {
        id: fire_radio
        x: 191
        y: 124
        text: qsTr("Fire Alarm")
        // checked: true
        onClicked: {
            console.log("Sound Fire Alarm selected")
            currentIndex = 2
            settings.sound_source = "sounds/Fire_Alarm-SoundBible.com-78848779.ogg"
        }
    }

    RadioButton {
        id: horn_radio
        x: 191
        y: 170
        text: qsTr("Fog Horn")
        onClicked: {
            console.log("Sound Fog Horn selected")
            currentIndex = 3
            settings.sound_source = "sounds/foghorn-daniel_simon.ogg"
        }
    }

    RadioButton {
        id: buzzer_radio
        x: 352
        y: 124
        text: qsTr("Clock Buzzer")
        onClicked: {
            console.log("Sound Alarm Clock selected")
            currentIndex = 4
            settings.sound_source = "sounds/Loud_Alarm_Clock_Buzzer-Muk1984-493547174.ogg"
        }
    }

    RadioButton {
        id: siren_radio
        x: 352
        y: 170
        text: qsTr("Siren")
        onClicked: {
            console.log("Sound Siren selected")
            currentIndex = 5
            settings.sound_source = "sounds/Siren_Noise-KevanGC-1337458893.ogg"
        }
    }

    Text {
        id: ring_volume_lbl1
        x: 35
        y: 328
        text: qsTr("Black Theme")
        font.pixelSize: 28
    }

    Switch {
        id: themeBlack
        x: 221
        y: 328
        text: themeBlack.value ? 'On' : 'Off'
    }

}
