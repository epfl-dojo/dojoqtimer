#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2>

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    app.setOrganizationName("EPFL Dojo");
    app.setOrganizationDomain("epfl.ch");
    app.setApplicationName("Dojo Timer");

    // https://doc.qt.io/qt-5/qtquickcontrols2-styles.html#
    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
