import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs.qml 1.0
import Qt.labs.settings 1.0
import QtMultimedia 5.12
import Qt.labs.platform 1.0
import "Utils.js" as Utils

Window {
    visible: true
    width: 640
    height: 300
    title: qsTr("Dojo Timer")
    property int countdown: settings.duration * 60 * 1000

    Settings {
        id: settings
        property int duration: 5
        property int volume: 50
        property string sound_source: "sounds/analog-watch-alarm_daniel-simion.ogg"
    }

    Audio {
        id: alarm
        source: settings.sound_source // "sounds/analog-watch-alarm_daniel-simion.ogg"
        volume: settings.volume / 100
    }

    Button {
        id: preferences_btn
        x: 513
        y: 27
        text: qsTr("Preferences")
        onClicked: {
            var component = Qt.createComponent("Preferences.qml")
            // https://stackoverflow.com/a/18025964/960623
            // you should always assert component.status === Component.Ready before calling createObject()
            if (component.status !== Component.Ready) {
                if (component.status === Component.Error) {
                    console.debug("Error:" + component.errorString());
                }
                return; // or maybe throw
            }
            var pref_window = component.createObject("preferences")
            pref_window.show()
        }
    }

    Text {
        id: chrono
        x: 0
        y: 78
        width: 640
        height: 134
        text: Utils.format_countdown(countdown)
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideMiddle
        font.pixelSize: 100
    }

    Timer {
        id: timer
        interval: 100; running: false; repeat: true
        onTriggered: {
            if (countdown <= 0) {
                timer.running = false
                alarm.play()
            } else {
            // 100 times faster for debbugging:
            // countdown = countdown - interval * 100
            countdown = countdown - interval
            }
        }
    }

    Button {
        id: toggle_btn
        x: 148
        y: 221
        text: timer.running ? qsTr("Pause") : qsTr("Start")
        onClicked: {
            timer.running = !timer.running
            if (countdown <= 0) {
                countdown = settings.duration * 60 * 1000
            }
        }
    }

    Button {
        id: reset_btn
        x: 407
        y: 221
        text: qsTr("Reset")
        onClicked: {
            timer.running = false
            countdown = settings.duration * 60 * 1000
            alarm.stop()
        }
    }

    ProgressBar {
        id: countdownBar
        x: 0
        y: 294
        width: parent.width
        height: 6
        value: 1.0 - countdown / (settings.duration * 60 * 1000)

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: parent.height
            color: "#e6e6e6"
            radius: 0
        }

        contentItem: Item {
            implicitWidth: parent.width
            implicitHeight: 4

            Rectangle {
                width: countdownBar.value * parent.width
                height: parent.height
                radius: 2
                color: "#E91E63"
            }
        }
    }
    SystemTrayIcon {
        visible: true
        iconSource: "qrc:/images/dojoman-small.svg"
        onMessageClicked: console.log("Message clicked")
        Component.onCompleted: showMessage("Message title", "Something important came up. Click this to know more.")
        onActivated: {
            console.log("Ouille")
        }
    }
}
