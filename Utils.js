.pragma library

function format_countdown(countdown) {
    let minutes = Math.floor(countdown / 60000)
    let seconds = Math.floor((countdown % 60000) / 1000)
    let tenthseconds = (countdown % 1000) / 100
    return (seconds === 60 ? (minutes+1) + ":00" : minutes + ":" + (seconds < 10 ? "0" : "") + seconds + '.' + tenthseconds)
}
